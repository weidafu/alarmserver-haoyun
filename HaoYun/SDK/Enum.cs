﻿using System.ComponentModel;

namespace Xunmei.AlarmServer.Device.HaoYun
{
    public enum eDeviceMessageType
    {
        [Description("未知消息类型")]
        DMTUnknow = -1,                                 //未知消息类型
        [Description("*HostOnLine：设备连接入网（推送状态）")]
        DMTLogin = 0,                                   //设备连接入网（推送状态）
        [Description("*HostOffLine：设备断开网络连接（推送状态）")]
        DMTLogout = 1,                                  //设备断开网络连接（推送状态）
        [Description("*HostArmedByRemote：远程布防操作成功")]
        DMTRemoteArmSuccess = 2,                        //远程布防操作成功
        [Description("*HostArmedByRemote：远程布防操作失败")]
        DMTRemoteArmFailed = 3,                         //远程布防操作失败
        [Description("*HostArmedByRemote：远程半布防操作成功")]
        DMTRemoteHalfArmSuccess = 4,                    //远程半布防操作成功
        [Description("*HostArmedByRemote：远程半布防操作失败")]
        DMTRemoteHalfArmFailed = 5,                     //远程半布防操作失败
        [Description("*HostDisArmed：撤防操作成功")]
        DMTRemoteDisarmSuccess = 6,                     //撤防操作成功
        [Description("*HostDisarmedByRemote：远程撤防操作失败")]
        DMTRemoteDisarmFailed = 7,                      //远程撤防操作失败
        [Description("消息回调设备当前配置信息给服务器")]
        DMTDeviceParameterInfo = 8,                     //消息回调设备当前配置信息给服务器
        [Description("消息回调设备当前防区信息给服务器")]
        DMTDeviceZoneInfo = 9,                          //消息回调设备当前防区信息给服务器
        [Description("设备语音参数成功")]
        DMTParameterSoundSuccess = 10,                  //设备语音参数成功
        [Description("设备语音参数失败")]
        DMTParameterSoundFailed = 11,                   //设备语音参数失败
        [Description("定时布撤防时段设置成功")]
        DMTParameterTimedArmPeriodGroupSuccess = 12,    //定时布撤防时段设置成功
        [Description("定时布撤防时段设置失败")]
        DMTParameterTimedArmPeriodGroupFailed = 13,     //定时布撤防时段设置失败
        [Description("接警电话号码设置成功")]
        DMTParameterAnswerNoSuccess = 14,               //接警电话号码设置成功
        [Description("接警电话号码设置失败")]
        DMTParameterAnswerNoFailed = 15,                //接警电话号码设置失败
        [Description("电话报警参数设置成功")]
        DMTParameterGSMAlarmSuccess = 16,               //电话报警参数设置成功
        [Description("电话报警参数设置失败")]
        DMTParameterGSMAlarmFailed = 17,                //电话报警参数设置失败
        [Description("报警参数设置成功")]
        DMTParameterAlarmSettingSuccess = 18,           //报警参数设置成功
        [Description("报警参数设置失败")]
        DMTParameterAlarmSettingFailed = 19,            //报警参数设置失败
        [Description("网络参数成功")]
        DMTParameterNetSuccess = 20,                    //网络参数成功
        [Description("网络参数失败")]
        DMTParameterNetFailed = 21,                     //网络参数失败
        [Description("进入对码状态")]
        DMTEnterZoneCode = 22,                          //进入对码状态
        [Description("对码操作成功")]
        DMTZoneCodeSuccess = 23,                        //对码操作成功
        [Description("对码操作超时")]
        DMTZoneCodeOverTime = 24,                       //对码操作超时
        [Description("删除对码成功")]
        DMTDeleteZoneCodeSuccess = 25,                  //删除对码成功
        [Description("设置防区属性成功")]
        DMTSetZonePropertySuccess = 26,                 //设置防区属性成功
        [Description("开始升级")]
        DMTStartUpgrade = 30,                           //开始升级
        [Description("未能进入版本升级：升级包跟当前软件版本一致")]
        DMTUpgradeFailedForSameVersion = 31,            //未能进入版本升级：升级包跟当前软件版本一致
        [Description("未能进入版本升级：升级包错误")]
        DMTUpgradeFailedForFileError = 32,              //未能进入版本升级：升级包错误
        [Description("升级成功，设备自动断线，即将重启")]
        DMTUpgradeSuccess = 33,                         //升级成功，设备自动断线，即将重启
        [Description("升级超时，设备退出升级状态")]
        DMTUpgradeOvertime = 34,                        //升级超时，设备退出升级状态
        [Description("升级进度")]
        DMTUpgradeProgress = 35,                        //升级进度
        [Description("设备状态发生改变")]
        DMTStatusChanged = 36,                          //设备状态发生改变
        [Description("下发备份的参数信息成功")]
        DMTDownloadParameterSuccess = 37,               //下发备份的参数信息成功
        [Description("参数信息发生改变")]
        DMTParameterChanged = 38,                       //参数信息发生改变
        [Description("卷帘门操作成功")]
        DMTRollingShutterDoorControlSuccess = 39,       //卷帘门操作成功
        [Description("卷帘门操作失败")]
        DMTRollingShutterDoorControlFailed = 40,        //卷帘门操作失败
        [Description("卷帘门操作超时")]
        DMTRollingShutterDoorControlOverTime = 41,      //卷帘门操作超时
        [Description("卷帘门无设备")]
        DMTRollingShutterDoorControlNoDevice = 42,      //卷帘门无设备
        [Description("防区状态发生改变")]
        DMTZoneStatusChanged = 43,                      //防区状态发生改变
        [Description("防区故障发生改变")]
        DMTZoneFaultChanged = 44,                       //防区故障发生改变
        [Description("扩展设备状态发生改变0：警号是否开启，1：联动继电器是否开启，2：警示牌指示是否开启")]
        DMTExpanderDevicesStatusChanged = 45,           //扩展设备状态发生改变0：警号是否开启，1：联动继电器是否开启，2：警示牌指示是否开启
        [Description("卷帘门对码成功")]
        DMTRollingShutterDoorCodeSuccess = 46,          //卷帘门对码成功
        [Description("卷帘门对码超时")]
        DMTRollingShutterDoorCodeOutTime = 47,          //卷帘门对码超时
        [Description("卷帘门无设备")]
        DMTRollingShutterDoorCodeNoDevice = 48,         //卷帘门无设备
        [Description("防区开关量发生改变")]
        DMTZoneSwitchChanged = 49,                      //防区开关量发生改变
        [Description("防区开关量发生改变")]
        DMTParameterIndependentParameterSuccess = 50,   //独立参数信息设置成功
        [Description("独立参数信息设置失败")]
        DMTParameterIndependentParameterFailed = 51,    //独立参数信息设置失败
        [Description("无线遥控设备操作回复")]
        DMTWirelessRemoteControl = 52,                  //无线遥控设备操作回复
        [Description("AT命令下发回复")]
        DMTATCommandRespond = 53,                       //AT命令下发回复
        [Description("收到校验码")]
        DMTLockRandomCode = 54,                         //收到校验码
        [Description("指纹锁的状态发生改变")]
        DMTLockStatusChanged = 55,                      //指纹锁的状态发生改变
        [Description("钥匙开锁")]
        DMTKeyUnlock = 56,                              //钥匙开锁
        [Description("指纹开锁")]
        DMTFingerprintUnlock = 57,                      //指纹开锁
        [Description("卡开锁")]
        DMTCardUnlock = 58,                             //卡开锁
        [Description("收到校验码")]
        DMTPasswordUnlock = 59,                         //密码开锁	
        [Description("密码开锁")]
        DMTTempCardUnlock = 60,                         //临时卡开锁
        [Description("收到校验码")]
        DMTTempPasswordUnlock = 61,                     //临时密码开锁	
        [Description("临时密码开锁")]
        DMTMaliciousTryLock = 62,                       //恶意试锁
        [Description("胁迫开锁")]
        DMTDuressUnlock = 64,                           //胁迫开锁
        [Description("探测器低压")]
        DMTDectorLow = 65,                              //探测器低压
        [Description("探测器失联")]
        DMTDectorLose = 66,                             //探测器失联
        [Description("探测器失联恢复")]
        DMTDectorLoseRecovery = 67,                     //探测器失联恢复
        [Description("*DeviceTamper：探测器防拆报警")]
        DMTDectorIllegalOpen = 68,                      //探测器防拆报警
        [Description("智能锁删除用户")]
        DMTIntelLockDelUser = 69,                       //智能锁删除用户
        [Description("智能锁发送一条数据")]
        DMTIntelLockSendData = 70,                      //智能锁发送一条数据
        [Description("收到设备总线状态")]
        DMTBusHostState = 71,                           //收到设备总线状态
        [Description("上线报告")]
        DMTOnLineReport = 72,                           //上线报告
        [Description("收到消防栓下发数据的回复")]
        DMTFireControlDeviceReceiveData = 73,           //收到消防栓下发数据的回复
        [Description("收到NB设备心跳")]
        DMTDeviceNBHeartbeat = 74,                      //收到NB设备心跳
        [Description("指纹锁滑盖开启")]
        DMTLockCoverOpen = 75,                          //指纹锁滑盖开启
        [Description("访客指纹开始")]
        DMTGuestFingerprintUnlock = 76,                 //访客指纹开始
        [Description("收到911NB下发数据的回复")]
        DMT911NBReceiveData = 77,                       //收到911NB下发数据的回复
        [Description("独立参数信息改变")]
        DMTParameterIndependentParameterChange = 78,    //独立参数信息改变
        [Description("事件同步异常")]
        DMTSynEventErr = 79,                            //事件同步异常
        [Description("防区同步异常")]
        DMTSynZoneErr = 80,                             //防区同步异常
        [Description("参数同步异常")]
        DMTSynParameErr = 81,                           //参数同步异常
        [Description("清除Flash")]
        DMTClearFlash = 82,                             //清除Flash
        [Description("网络制式改变")]
        DMTNetworkStandardChange = 83,                  //网络制式改变
        [Description("网络运营商改变")]
        DMTNetworkOperatorChange = 84,                  //网络运营商改变
        [Description("网卡信息1改变")]
        DMTNetworkCard1Change = 85,                     //网卡信息1改变
        [Description("网卡信息2改变")]
        DMTNetworkCard2Change = 86,                     //网卡信息2改变
        [Description("主机制造商改变")]
        DMTHostManufacturerChange = 87,                 //主机制造商改变
        [Description("主机型号改变")]
        DMTHostModelChange = 88,                        //主机型号改变
        [Description("主机基础信息改变")]
        DMTHostBasicInfoChange = 89,                    //主机基础信息改变
        [Description("扩展模块编号改变")]
        DMTExpansionModuleNoChange = 90,                //扩展模块编号改变
        [Description("查询扩展模块信息改变")]
        DMTExpansionModuleInfoChange = 91,              //查询扩展模块信息改变
        [Description("分区布撤防改变")]
        DMTZoneArmChange = 92,                          //分区布撤防改变
        [Description("继电器设置成功")]
        DMTRelaySettingSuccess = 93,                    //继电器设置成功
        [Description("主机类型改变")]
        DMTHostTypeChange = 94,                         //主机类型改变
        [Description("普通继电器设置成功")]
        DMTOrdinaryRelaySettingSuccess = 95,            //普通继电器设置成功
        [Description("历史布撤防事件查询成功")]
        DMTEventArmQuerySuccess = 96,                   //历史布撤防事件查询成功
        [Description("历史报警事件查询成功")]
        DMTEventAlarmQuerySuccess = 97,					//历史报警事件查询成功
    };

    public enum eDeviceEventType
    {
        [Description("未知事件类型")]
        DETUnknow = -1,                                 //未知事件类型
        [Description("*HostArmedByRemote：远程布防（手机、PC）（推送状态、操作者：APP账号）")]
        DETRemoteArm = 0,                               //远程布防（手机、PC）（推送状态、操作者：APP账号）
        [Description("*HostDisarmedByRemote：远程撤防（手机、PC）（推送状态、操作者：APP账号）")]
        DETRemoteDisarm = 1,                            //远程撤防（手机、PC）（推送状态、操作者：APP账号）
        [Description("*HostArmedByRemote：远程半布防（手机、PC）（推送状态、操作者：APP账号）")]
        DETRemoteHalfArm = 2,                           //远程半布防（手机、PC）（推送状态、操作者：APP账号）
        [Description("*HostArmed:定时布防（推送状态、操作者：无）")]
        DETTimedArm = 3,                                //定时布防（推送状态、操作者：无）
        [Description("*HostDisArmed：定时撤防（推送状态、操作者：无）")]
        DETTimedDisarm = 4,                             //定时撤防（推送状态、操作者：无）
        [Description("*HostArmed:定时半布防（推送状态、操作者：无）")]
        DETTimedHalfArm = 5,                            //定时半布防（推送状态、操作者：无）
        [Description("*HostArmed：本地布防（遥控器）（推送状态、操作者：1-8号遥控器）")]
        DETLocalArm = 6,                                //本地布防（遥控器）（推送状态、操作者：1-8号遥控器）
        [Description("*HostDisArmed：本地撤防（遥控器）（推送状态、操作者：1-8号遥控器）")]
        DETLocalDisarm = 7,                             //本地撤防（遥控器）（推送状态、操作者：1-8号遥控器）
        [Description("*HostArmed：本地半布防（遥控器）（推送状态、操作者：1-8号遥控器）")]
        DETLocalHalfArm = 8,                            //本地半布防（遥控器）（推送状态、操作者：1-8号遥控器）
        [Description("*HostArmed：短信布防（推送状态、操作者：无）")]
        DETShortMsgArm = 9,                             //短信布防（推送状态、操作者：无）
        [Description("*HostDisArmed：短信撤防（推送状态、操作者：无）")]
        DETShortMsgDisarm = 10,                         //短信撤防（推送状态、操作者：无）
        [Description("*HostArmed：短信半布防（推送状态、操作者：无）")]
        DETShortMsgHalfArm = 11,                        //短信半布防（推送状态、操作者：无）
        [Description("*HostArmed:电话布防（拨号布防）（推送状态、操作者：无）")]
        DETGSMArm = 12,                                 //电话布防（拨号布防）（推送状态、操作者：无）
        [Description("*HostDisArmed：电话撤防（拨号撤防）（推送状态、操作者：无）")]
        DETGSMDisarm = 13,                              //电话撤防（拨号撤防）（推送状态、操作者：无）
        [Description("*HostArmed：电话半布防（拨号撤防）（推送状态、操作者：无）")]
        DETGSMHalfArm = 14,                             //电话半布防（拨号撤防）（推送状态、操作者：无）
        [Description("*HostArmed：自动布防（智能布防）（推送状态、操作者：无）")]
        DETAutoArm = 15,                                //自动布防（智能布防）（推送状态、操作者：无）
        [Description("*HostDisArmed：自动撤防（智能撤防）（推送状态、操作者：无）")]
        DETAutoDisarm = 16,                             //自动撤防（智能撤防）（推送状态、操作者：无）
        [Description("*HostArmed：自动半布防（智能半布防）（推送状态、操作者：无）")]
        DETAutoHalfArm = 17,                            //自动半布防（智能半布防）（推送状态、操作者：无）
        [Description("*HostAlarmOccured：无线防区报警（推送报警、cZoneIndex（1-99）表示几号防区触发报警）")]
        DETZoneAlarm = 18,                              //无线防区报警（推送报警、cZoneIndex（1-99）表示几号防区触发报警）
        [Description("遥控器报警（推送报警、cZoneIndex（1-8）表示几号遥控器触发报警）")]
        DETRemoteControllerAlarm = 19,                  //遥控器报警（推送报警、cZoneIndex（1-8）表示几号遥控器触发报警）
        [Description("*DeviceTamper：防拆报警（报警设备非法打开）")]
        DETIllegalOpenAlarm = 20,                       //防拆报警（报警设备非法打开）
        [Description("接警，cZoneIndex=1：表示静音接警不撤防，2：表示静音不接警不撤防，3：表示静音接警并撤防")]
        DETAnswerAlarm = 21,                            //接警，cZoneIndex=1：表示静音接警不撤防，2：表示静音不接警不撤防，3：表示静音接警并撤防
        [Description("防拆恢复")]
        DETRecoveryIllegalOpen = 22,                    //防拆恢复
        [Description("*ACFault：市电掉电")]
        DETACOff = 23,                                  //市电掉电
        [Description("*ACRecovery：市电恢复")]
        DETACRecovery = 24,                             //市电恢复
        [Description("*BatteryFailure：电池低压")]
        DETBatteryLow = 25,                             //电池低压
        [Description("*BatteryRecovery：电池恢复")]
        DETBatteryRecovery = 26,                        //电池恢复
        [Description("探测器低压")]
        DETDectorLow = 27,                              //探测器低压
        [Description("探测器失联")]
        DETDectorLose = 28,                             //探测器失联
        [Description("*DeviceTamper：探测器防拆报警")]
        DETDectorIllegalOpen = 29,                      //探测器防拆报警
        [Description("探测器交流停电")]
        DETDectorACOff = 30,                            //探测器交流停电
        [Description("探测器交流恢复")]
        DETDectorACRecovery = 31,                       //探测器交流恢复
        [Description("探测器故障")]
        DETDectorFault = 32,                            //探测器故障
        [Description("恢复出厂设置")]
        DETDeviceRecovery = 33,                         //恢复出厂设置
        [Description("GSM故障")]
        DETGSMFault = 34,                               //GSM故障
        [Description("GSM故障恢复")]
        DETGSMRecovery = 35,                            //GSM故障恢复
        [Description("设备开机")]
        DETDeviceOpen = 36,                             //设备开机
        [Description("门铃触发事件")]
        DETDoorbellRing = 37,                           //门铃触发事件
        [Description("*InputActivity|HostAlarmOccured：有线防区报警")]
        DETWireZoneAlarm = 38,                          //有线防区报警
        [Description("*InputBypassed：有线防区旁路")]
        DETWireZoneNeglect = 39,                        //有线防区旁路
        [Description("*InputBypassed：无线防区旁路")]
        DETZoneNeglect = 40,                            //无线防区旁路
        [Description("*InputNormaled：有线旁路恢复")]
        DETWireZoneNeglectRecovery = 41,                //有线旁路恢复
        [Description("*InputNormaled：无线旁路恢复")]
        DETZoneNeglectRecovery = 42,                    //无线旁路恢复
        [Description("有线防区故障")]
        DETWireZoneFault = 43,                          //有线防区故障
        [Description("无线防区故障")]
        DETZoneFault = 44,                              //无线防区故障
        [Description("有线防区故障恢复")]
        DETWireZoneFaultRecovery = 45,                  //有线防区故障恢复
        [Description("无线防区故障恢复")]
        DETZoneFaultRecovery = 46,                      //无线防区故障恢复
        [Description("*HostArmedByKeyboard：键盘布防（推送状态、操作者：无）")]
        DETKeyboardArm = 47,                            //键盘布防（推送状态、操作者：无）
        [Description("*HostDisarmedByKeyboard：键盘撤防（推送状态、操作者：无）")]
        DETKeyboardDisarm = 48,                         //键盘撤防（推送状态、操作者：无）
        [Description("*HostArmedByKeyboard：键盘半布防（推送状态、操作者：无）")]
        DETKeyboardHalfArm = 49,                        //键盘半布防（推送状态、操作者：无）
        [Description("*HostAlarmOccured：键盘紧急报警")]
        DETKeyboardAlarm = 50,                          //键盘紧急报警
        [Description("*DeviceTamper：键盘防拆报警")]
        DETKeyboardIllegalOpenAlarm = 51,               //键盘防拆报警
        [Description("探测器上线 只有无线防区")]
        DETDetectorOnline = 52,                         //探测器上线 只有无线防区
        [Description("无线遥控插座开")]
        DETWirelessSocketOpen = 53,                     //无线遥控插座开
        [Description("无线遥控插座关")]
        DETWirelessSocketClose = 54,                    //无线遥控插座关
        [Description("电缆防区故障")]
        DETCableZoneFault = 56,                         //电缆防区故障
        [Description("设备故障")]
        DETDeviceFault = 57,                            //设备故障
        [Description("设备故障恢复")]
        DETDeviceFaultRecovery = 58,                    //设备故障恢复
        [Description("钥匙开锁")]
        DETKeyUnlock = 59,                              //钥匙开锁
        [Description("指纹开锁")]
        DETFingerprintUnlock = 60,                      //指纹开锁
        [Description("卡开锁")]
        DETCardUnlock = 61,                             //卡开锁
        [Description("密码开锁")]
        DETPasswordUnlock = 62,                         //密码开锁	
        [Description("临时卡开锁")]
        DETTempCardUnlock = 63,                         //临时卡开锁
        [Description("临时卡开锁")]
        DETTempPasswordUnlock = 64,                     //临时密码开锁	
        [Description("恶意试锁")]
        DETMaliciousTryLock = 65,                       //恶意试锁
        [Description("烟感报警")]
        DETSmokeAlarm = 66,                             //烟感报警
        [Description("气感报警")]
        DETGasAlarm = 67,                               //气感报警
        [Description("水浸报警")]
        DETWaterAlarm = 68,                             //水浸报警
        [Description("水浸报警恢复")]
        DETWaterAlarmRecovery = 69,                     //水浸报警恢复
        [Description("胁迫开锁")]
        DETDuressUnlock = 70,                           //胁迫开锁
        [Description("人员巡更")]
        DETGuardPatrol = 71,                            //人员巡更
        [Description("指纹锁滑盖开启")]
        DETLockCoverOpen = 72,                          //指纹锁滑盖开启
        [Description("访客指纹开始")]
        DETGuestFingerprintUnlock = 73,                 //访客指纹开始
        [Description("*HostAlarmOccured：911NB紧急报警")]
        DET911NBUrgentAlarm = 74,                       //911NB紧急报警
        [Description("*InputActivity|HostAlarmOccured：总线防区报警")]
        DETBusZoneAlarm = 75,                           //总线防区报警
        [Description("*InputBypassed：总线防区旁路")]
        DETBusZoneNeglect = 76,                         //总线防区旁路
        [Description("*InputNormaled：总线旁路恢复")]
        DETBusZoneNeglectRecovery = 77,                 //总线旁路恢复
        [Description("总线防区故障")]
        DETBusZoneFault = 78,                           //总线防区故障
        [Description("总线防区故障恢复")]
        DETBusZoneFaultRecovery = 79,                   //总线防区故障恢复
        [Description("*InputNormaled：总线防区报警恢复")]
        DETBusZoneAlarmRecovery = 80,                   //总线防区报警恢复
        [Description("*InputNormaled：有线防区报警恢复")]
        DETWireZoneAlarmRecovery = 81,                  //有线防区报警恢复
        [Description("*InputNormaled：无线防区报警恢复")]
        DETZoneAlarmRecovery = 82,                      //无线防区报警恢复
        [Description("无线探测器失联恢复")]
        DETDectorLoseRecovery = 83,                     //无线探测器失联恢复
        [Description("进入编程")]
        DETInSetting = 100,                             //进入编程
        [Description("退出编程")]
        DETOutSetting = 101,                            //退出编程
        [Description("手动触发测试")]
        DETManualTest = 102,                            //手动触发测试
        [Description("*HostAlarmOccured：胁迫报警")]
        DETDuressAlarm = 103,                           //胁迫报警
        [Description("*HostOutputTurnOn：特殊用途继电器开")]
        DETSpecialRelay = 104,                          //特殊用途继电器开
        [Description("*HostOutputTurnOff：特殊用途继电器关")]
        DETSpecialRelayClose = 105,                     //特殊用途继电器关
        [Description("*PhoneLineFailure：电话线故障")]
        DETPhoneLineFault = 106,                        //电话线故障
        [Description("*PhoneLineRecovery：电话线故障恢复")]
        DETPhoneLineFaultRecovery = 107,                //电话线故障恢复
        [Description("键盘接警，cZoneIndex代表键盘编号， u8Account 1：表示静音接警不撤防，2：表示静音不接警不撤防，3：表示静音接警并撤防")]
        DETKeyboardAnswerAlarm = 108,                   //键盘接警，cZoneIndex代表键盘编号， u8Account 1：表示静音接警不撤防，2：表示静音不接警不撤防，3：表示静音接警并撤防
        [Description("*PhoneLineFailure：电话通讯失败")]
        DETPhoneCommunicationFail = 109,                //电话通讯失败
        [Description("*PhoneLineRecovery：电话通讯恢复")]
        DETPhoneCommunicationRecovery = 110,            //电话通讯恢复
        [Description("*ModuleFailure：扩展防区模块故障")]
        DETExpansionZoneFault = 111,					//扩展防区模块故障
    };

    /*属性名称*/
    public enum eDevicePropertyName
    {
        DPN_IsOnline = 0,                               //查询查询指定设备是否在线（DPTBool）
        DPN_DeviceSN = 1,                               //查询设备序列号（DPTString）
        DPN_DeviceIP = 2,                               //查询设备IP地址（DPTString）
        DPN_DeviceUDPPort = 3,                          //查询设备UDP端口（DPTInt）
        DPN_DeviceTCPPort = 4,                          //查询设备TCP端口（DPTInt）
        DPN_DevicePanelNo = 5,                          //查询设备编号（DPTString）
        DPN_Version = 6,                                //查询版本（DPTInt）
        DPN_DeviceSymbol = 7,                           //查询设备标识符（DPTInt）
        DPN_ACStatus = 8,                               //查询交流电状态（DPTACStatus）
        DPN_BatteryStatus = 9,                          //查询电池状态（eBatteryStatus）
        DPN_ArmStatus = 10,                             //查询布撤防状态（eArmStatus）
        DPN_AlarmStatus = 11,                           //查询报警状态（eAlarmStatus）
        DPN_SignalStatus = 12,                          //查询信号强度（eNetSignalStatus）
        DPNParameterSound = 20,                         //查询\设置设备语音参数（qrParameterSound）
        DPNParameterTimedArmPeriodGroup = 21,           //查询\设置定时布撤防时段（qrParameterTimedArmPeriodGroup）
        DPNParameterAnswerNo = 22,                      //查询\设置接警电话号码（qrParameterAnswerNo）
        DPNParameterGSMAlarm = 23,                      //查询\设置电话报警参数（qrParameterGSMAlarm）
        DPNParameterAlarmSetting = 24,                  //查询\设置报警参数（qrParameterAlarmSetting）
        DPNParameterNet = 25,                           //查询\设置网络参数（qrParameterNet）
        DPNAllInfo = 26,                                //查询\设置所有参数信息（qrDeviceTemplate）
        DPNAllZone = 27,                                //查询\设置所有防区信息（qrDeviceTemplate）
        DPNZoneProperty = 28,                           //设置设置防区属性(qrDeviceZoneProperty)
        DPN_WirelessZone = 29,                          //获取所有无线防区（qrDeviceZoneProperty）
        DPN_RemoteControlZone = 30,                     //获取所有遥控防区（qrDeviceZoneProperty）
        DPNRemoteUpdate = 31,                           //远程升级
        DPNLastReceiveTimeStamp = 32,                   //获取最后通信时间
        DPNSyncDateTime = 33,                           //设置同步时间
        DPNAllZoneClear = 34,                           //清除所有防区信息
        DPNWirelessZoneClear = 35,                      //清除所有无线防区（qrDeviceZoneProperty）
        DPNRemoteControlZoneClear = 36,                 //清除所有遥控防区（qrDeviceZoneProperty）
        DPN_WireZone = 37,                              //获取所有有线防区（qrDeviceZoneProperty）
        DPN_CameraSN = 38,                              //获取设备的视频序列号（DPTString）
        DPN_IsUpdate = 39,                              //获取是否正在升级以及升级进度
        DPNRollingShutterDoorControl = 40,              //控制卷帘门
        DPN_WireZoneAndStatus = 41,                     //获取所有有线防区&是否故障(qrDeviceZonePropertyAndFault，qrDeviceZonePropertyAndFault216GPlus)
        DPN_WirelessZoneAndStatus = 42,                 //获取所有无线防区&是否故障(qrDeviceZonePropertyAndFault，qrDeviceZonePropertyAndFault216GPlus)
        DPN_DevicePartsStatus = 43,                     //获取设备的硬件状态，0：警号是否开启，1：联动继电器是否开启，2：警示牌指示是否开启(qrDeviceZoneFault)
        DPNRollingShutterDoorCode = 44,                 //卷帘门对码
        DPN_FirmwareVersion = 45,                       //查询设备固件版本（DPTString）
        DPNIndependentParameter = 46,                   //查询独立参数信息（qrParameterIndependentParameter）
        DPNIPStopDevice = 47,                           //查询\设置，独立参数信息:设备停用（DPTUnsignedChar）
        DPNIPServiceSendAlarmInfo = 48,                 //查询\设置，独立参数信息:中心下发组网报警信息（qrParameterServiceSendAlarmInfo）
        DPNWirelessSocketControl = 49,                  //设置无线遥控插座开/关(qrPowerControl)
        DPN_DeviceVersion = 50,                         //设备版本信息
        DPNWirelessRemoteControl = 51,                  //无线遥控设备操作(qrParameterWirelessRemoteControl)
        DPNReturnHeartbeat = 52,                        //设置报警心跳是否回复
        DPNATCommand = 53,                              //查询发送AT命令和返回结果
        DPNWiredZoneProperty = 54,                      //设置有线防区属性(qrDeviceZoneProperty，qrDeviceZonePropertyAndFault216GPlus)
        DPNWirelessZoneProperty = 55,                   //设置无线防区属性(qrDeviceZoneProperty，qrDeviceZonePropertyAndFault216GPlus)
        DPNRemoteZoneProperty = 56,                     //设置遥控防区属性(qrDeviceZoneProperty，qrDeviceZonePropertyAndFault216GPlus)
        DPNSettingTempPassword = 57,                    //设置临时授权码(qrSettingPassWordParam)
        DPNOneKeyRemoteControl = 58,                    //获取单键遥控列表
        DPNDoubleKeyRemoteControl = 59,                 //获取双键遥控列表
        DPNWindowsRemoteControl = 60,                   //获取遥控窗帘列表
        DPNFingerprintLock = 61,                        //获取指纹锁列表(qrOtherZoneInfo)
        DPNSettingAd = 62,                              //广告(qrSettingAdParam)
        DPNRandomCode = 63,                             //关联设备验证码(qrRandomCodeParam)
        DPNSettingLockUserName = 64,                    //指纹锁用户名设置(qrSettingLockUserNameParam)
        DPNFingerprintLockStatus = 65,                  //获取指纹锁状态列表(qrLockStateParam)
        DPNFireHydrantStatus = 66,                      //获取消防栓参数(qrFireHydrantParam)
        DPNLockLastReceiveTimeStamp = 67,               //获取智能锁最后通信时间(qrIntelLockLastReceiveTime)
        DPNWireZoneBusHost = 68,                        //获取所有有线防区总线主机的实时状态(qrZoneBusHostState)
        DPNWirelessZoneBusHost = 69,                    //获取所有无线防区总线主机的实时状态(qrZoneBusHostState)
        DPNAutomaticFireStatus = 70,                    //获取自动灭火参数(qrAutomaticFireParam)
        DPNFireHydrantSendData = 71,                    //消防栓下发数据(qrFireControlDeviceSendData)
        DPNAutomaticFireSendData = 72,                  //自动灭火设备下发数据(qrFireControlDeviceSendData)
        DPNHeartbeatInterval = 73,                      //获取心跳间隔时间(DPTInt)
        DPNFireHydrantIMEI = 74,                        //获取IMEI(DPTUnsignedChar)
        DPNFireHydrantIMSI = 75,                        //获取IMSI(DPTUnsignedChar)
        DPNIPUserPassword = 76,                         //查询\设置，独立参数信息:设置用户密码（qrParameterUserPassword）
        DPN_DetectorBusZoneAndStatus = 77,              //获取所有总线防区&是否故障(qrDeviceZonePropertyAndFault，qrDeviceZonePropertyAndFault216GPlus)
        DPNDetectorBusZoneProperty = 78,                //设置总线防区属性(qrDeviceZoneProperty，qrDeviceZonePropertyAndFault216GPlus)
        DPN911NBSendData = 79,                          //911NB设备下发数据(qrFireControlDeviceSendData)
        DPN911NBStatus = 80,                            //获取911NB参数(qr911NBParam)
        DPN_NowEventNo = 81,                            //获取设备最新事件号
        DPNZoneTempNeglect = 82,                        //防区永久旁路(qrDeviceZoneTempNeglect)
        DPNSirenStatus = 83,                            //查询或设置警号状态，0关闭，1启动(DPTInt)
        DPN_NetworkStandard = 84,                       //获取网络制式(eNetworkStandard)
        DPN_NetworkOperator = 85,                       //获取网络运营商(eNetworkOperator)
        DPNClearFlash = 86,                             //清除设备Flash
        DPNNetworkCard1 = 87,                           //网卡信息1(qrParameterNetworkCard)
        DPNNetworkCard2 = 88,                           //网卡信息2(qrParameterNetworkCard)
        DPN_HostManufacturer = 89,                      //主机制造商(DPTUnsignedChar)
        DPN_HostModel = 90,                             //主机型号(DPTUnsignedChar)
        DPNRelayStatus = 91,                            //查询或设置继电器状态，0关闭，1启动(DPTInt)
        DPN_HostBasicInfo = 92,                         //查询主机基础信息(qrHostBasicInfo)
        DPN_ExpansionModuleNo = 93,                     //扩展模块编号(DPTUnsignedChar)
        DPN_ExpansionModuleInfo = 94,                   //查询扩展模块信息(qrExpansionModuleInfo)
        DPNZoneArm = 95,                                //分区布撤防(qrZoneArm)
        DPN_HostType = 96,                              //查询主机类型(eHostType)
        DPNOrdinaryRelay = 97,                          //普通继电器,地位四个字节代表可以操作的四个继电器(qrDeviceZoneFault)
        DPNEventArmQuery = 98,                          //查询历史布撤防事件(qrEventInfo/qrEventQuery)
        DPNEventAlarmQuery = 99,                        //查询历史报警事件(qrEventInfo/qrEventQuery)
    };

    /*属性值类型*/
    public enum eDevicePropertyType
    {
        DPTUnsignedChar = 0,                            //字节数组
        DPTInt = 1,
        DPTLong = 2,
        DPTFloat = 3,
        DPTDouble = 4,
        DPTString = 5,
        DPTBool = 6,                                    //BOOL
        DPTACStatus = 7,                                //eACStatus
        DPTBatteryStatus = 8,                           //eBatteryStatus
        DPTArmStatus = 9,                               //eArmStatus
        DPTAlarmStatus = 10,                            //eAlarmStatus
        DPTNetSignalStatus = 11,                        //eNetSignalStatus
        DPTParameterSound = 20,                         //语音参数结构体（qrParameterSound）
        DPTParameterTimedArmPeriodGroup = 21,           //定时布撤防结构体（qrParameterTimedArmPeriodGroup）
        DPTParameterAnswerNo = 22,                      //接警电话号码结构体（qrParameterAnswerNo）
        DPTParameterGSMAlarm = 23,                      //电话报警结构体（qrParameterGSMAlarm）
        DPTParameterAlarmSetting = 24,                  //报警参数结构体（qrParameterAlarmSetting）
        DPTParameterNet = 25,                           //网络参数结构体（qrParameterNet）
        DPTParameterTemplate = 26,                      //设备参数、防区信息备份、下载结构体（qrDeviceTemplate）
        DPTZoneProperty = 27,                           //防区参数设置（指定门铃、旁路、智能防区）（qrDeviceZoneProperty）
        DPTRollingShutterDoorControl = 28,              //卷帘门控制（qrParameterRollingShutterDoorControl）
        DPTDeviceZonePropertyAndFault = 29,             //防区参数&故障(qrDeviceZonePropertyAndFault)
        DPTDeviceZoneFault = 30,                        //防区&旁路结构体，一个字节8个位，8个防区(qrDeviceZoneFault)
        DPTIndependentParameter = 31,                   //独立参数信息(qrParameterIndependentParameter)
        DPTPowerControl = 32,                           //遥控插座控制(qrPowerControl)
        DPTWirelessRemoteControl = 33,                  //无线遥控设备操作(qrParameterWirelessRemoteControl)
        DPTSettingPassWordParam = 34,                   //设置远程授权码(qrSettingPassWordParam)
        DPTOtherZoneInfo = 35,                          //其他类型的的防区(qrOtherZoneInfo)
        DPTSettingAdParam = 36,                         //广告(qrSettingAdParam)
        DPTRandomCodeParam = 37,                        //关联设备验证码(qrRandomCodeParam)
        DPTSettingLockUserNameParam = 38,               //指纹锁用户名设置(qrSettingLockUserNameParam)
        DPTLockStateParam = 39,                         //指纹锁状态列表(qrLockStateParam)
        DPTFireHydrantParam = 40,                       //消防栓状态(qrFireHydrantParam)
        DPTIntelLockLastReceiveTime = 41,               //指纹锁最后通信时间(qrIntelLockLastReceiveTime)
        DPTZoneBusHostState = 42,                       //获取总线主机的实时状态(qrZoneBusHostState)
        DPTAutomaticFireParam = 43,                     //自动灭火状态(qrAutomaticFireParam)
        DPTFireControlDeviceSendData = 44,              //消防设备下发参数(qrFireControlDeviceSendData)
        DPTDeviceZonePropertyAndFault216GPlus = 45,     //防区参数&故障，216G+用(qrDeviceZonePropertyAndFault216GPlus)
        DPTParameterServiceSendAlarmInfo = 46,          //独立参数信息:中心下发组网报警信息(qrParameterServiceSendAlarmInfo)
        DPTqrParameterUserPassword = 47,                //独立参数信息:设置用户密码(qrParameterUserPassword)
        DPT911NBParam = 48,                             //911NB状态(qr911NBParam)
        DPTShort = 49,
        DPTZoneTempNeglect = 50,                        //防区永久旁路(qrDeviceZoneTempNeglect)
        DPTNetworkStandard = 51,                        //网络制式(eNetworkStandard)
        DPTNetworkOperator = 52,                        //网络运营商(eNetworkOperator)
        DPTNetworkCard = 53,                            //网卡信息(qrParameterNetworkCard)
        DPTHostBasicInfo = 54,                          //主机基础信息(qrHostBasicInfo)
        DPTExpansionModuleInfo = 55,                    //查询扩展模块信息(qrExpansionModuleInfo)
        DPTZoneArm = 56,                                //分区布撤防(qrZoneArm)
        DPTHostType = 57,                               //主机类型(eHostType)
        DPTEventInfo = 58,                              //事件内容结构体(qrEventInfo)
        DPTEventQuery = 59,                             //查询事件结构体(qrEventQuery)
    };

    //接警操作
    public enum eDeviceAnswerAlarm
    {
        DAAOnlyAnswer,                                  //只接警不撤防
        DAAOnlySilent,                                  //只静音
        DAADisarmAndRemove,                             //撤防及解除警号
    };

    /*错误代码*/
    public enum eQRErrorCode
    {
        EECNormal = 0x00,                               //没有错误发生
        EECServerDontService = 0x01,                    //服务器没有启动服务
        EECOffline = 0x02,                              //设备离线
        EECDeviceUpgrading = 0x03,                      //设备正在升级中、不允许对设备进行任何布撤防、设置操作
        EECParameterError = 0x04,                       //设置参数错误
        EECUpgradeFileError = 0x05,                     //升级文件错误
        EECSameUpgradeVersion = 0x06,                   //设备版本当当前版本一致，不进行升级
        EECAlreadyStartTransmit = 0x07,                 //已经启动转发服务功能
        EECUnSupportFunction = 0x08,                    //不支持操作命令	By Oscar 2016.11.12
        EECNoSelectedService = 0x09,                    /*没有启动任何服务*/
        EECPortBinded = 0x0A,                           /*端口已经被绑定*/
    };
}
