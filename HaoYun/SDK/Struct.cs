﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Xunmei.AlarmServer.Device.HaoYun
{
    /*分区布撤防状态*/
    [StructLayoutAttribute(LayoutKind.Sequential)]
    public struct qrZoneArm
    {
        /*分区编号*/
        public byte cZoneIndex;

        /*布撤防状态，0撤防，1布防*/
        public byte cArmStatus;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct qrDeviceZoneTempNeglect
    {
        /*防区编号*/
        public ushort cIndex;
        /*防区类型,1有线防区，2无线防区,3总线防区*/
        public byte cType;
        /*是否旁路 1 旁路 0 取消旁路*/
        public byte cNeglect;
    }

    //防区&旁路结构体，一个字节8个位，8个防区
    [StructLayout(LayoutKind.Sequential)]
    public struct qrDeviceZoneFault
    {
        public byte bFaultZone;
        ///*防区&旁路防区1*/
        //public byte bFaultZone1;
        ///*防区&旁路防区2*/
        //public byte bFaultZone2;
        ///*防区&旁路防区3*/
        //public byte bFaultZone3;
        ///*防区&旁路防区4*/
        //public byte bFaultZone4;
        ///*防区&旁路防区5*/
        //public byte bFaultZone5;
        ///*防区&旁路防区6*/
        //public byte bFaultZone6;
        ///*防区&旁路防区7*/
        //public byte bFaultZone7;
        ///*防区&旁路防区8*/
        //public byte bFaultZone8;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct qrZoneBusHostState
    {
        //防区号
        public byte cIndex;
        //bit0代表防区是否故障，bit1代表防区是否失联，bit2代表防区是否低压，bit3代表防区是否掉电，bit4代表防区是否布防，bit5代表是否旁路，bit6预留为0，bit7代表防区是否启用
        public byte state;
    }

    /*单个防区属性信息&故障&开关量，216G+*/
    [StructLayout(LayoutKind.Sequential)]
    public struct qrDeviceZonePropertyAndFault216GPlus
    {
        /*防区编号*/
        public byte cIndex;
        /*是否启用*/
        public byte bEnabled;
        /*布防延时是否有效*/
        public byte bArmDelay;
        /*是否为旁路防区*/
        public byte bNeglect;
        /*是否故障*/
        public byte bFault;
        /*是否为24小时防区*/
        public byte b24HourArm;
        /*报警是否现场语音播报*/
        public byte bAlarmSoundOut;
        /*是否联动防区*/
        public byte bLinkage;
        /*旁路状态*/
        public byte bCurrentNeglect;
        /*所属分区*/
        public byte cBelongPartition;
        /*防区联网代码*/
        public byte cNetCode;
        /*防区CID*/
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public byte[] cCid;
    }
}
