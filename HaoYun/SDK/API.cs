﻿using System;
using System.Text;
using System.Runtime.InteropServices;

namespace Xunmei.AlarmServer.Device.HaoYun
{
    public partial class SDK
    {
        private const string _dllPath = "HYSDK.dll";

        //[DllImportAttribute(_dllPath, EntryPoint = "?HYInitialize@@YG?B_NPBDAB_NABH1222ABQ6G?B_N0ABW4eDeviceMessageType@@AB_JPAIPAGPAEPADPAH6789@ZABQ6GX0ABW4eDeviceEventType@@ABGABE44_N@Z@Z")]
        [DllImportAttribute(_dllPath, EntryPoint = "HYInitialize")]
        [return: MarshalAsAttribute(UnmanagedType.Bool)]
        public static extern bool HYInitialize(string ip, ref bool isTranslateAlarm, ref int portPanel, ref bool isTranslateVideo, ref int portCameraCmd, ref int portCameraVideo, ref int portCameraAudio, ref HYDeviceMessage cbMessage, ref HYDeviceEvent cbEventEx);

        //[DllImportAttribute(_dllPath, EntryPoint = "?HYDeInitialize@@YG_NXZ")]
        [DllImportAttribute(_dllPath, EntryPoint = "HYDeInitialize")]
        [return: MarshalAsAttribute(UnmanagedType.Bool)]
        public static extern bool HYDeInitialize();

        //[DllImportAttribute(_dllPath, EntryPoint = "?HYGetDeviceProperty@@YGPAXPBDABW4eDevicePropertyName@@PAW4eDevicePropertyType@@PAHPA_NAB_J@Z")]
        [DllImportAttribute(_dllPath, EntryPoint = "HYGetDeviceProperty")]
        //[return: MarshalAsAttribute(UnmanagedType.LPStr)]
        //public static extern IntPtr HYGetDeviceProperty(string pDeviceSn, ref eDevicePropertyName eName, out eDevicePropertyType eType, out int iValueLen, out bool bValidateValue, ref Int64 u8Account);
        public static extern IntPtr HYGetDeviceProperty([In, MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder pDeviceSn,
            [In] ref eDevicePropertyName eName,
            [In] ref eDevicePropertyType eType,
            [In] ref int iValueLen,
            [In] ref bool bValidateValue,
            [In] ref int u8Account);

        //[DllImportAttribute(_dllPath, EntryPoint = "?HYSetDeviceProperty@@YG_NPBDABW4eDevicePropertyName@@ABW4eDevicePropertyType@@ABHPAXAB_J@Z")]
        [DllImportAttribute(_dllPath, EntryPoint = "HYSetDeviceProperty")]
        [return: MarshalAsAttribute(UnmanagedType.Bool)]
        public static extern bool HYSetDeviceProperty(string pDeviceSn, ref eDevicePropertyName eName, ref eDevicePropertyType eType, ref int iValueLen, IntPtr value, ref Int64 u8Account);

        //[DllImportAttribute(_dllPath, EntryPoint = "?HYDeviceAnswerAlarm@@YG_NPBDABW4eDeviceAnswerAlarm@@AB_J@Z")]
        [DllImportAttribute(_dllPath, EntryPoint = "HYDeviceAnswerAlarm")]
        [return: MarshalAsAttribute(UnmanagedType.Bool)]
        public static extern bool HYDeviceAnswerAlarm(string pDeviceSn, ref eDeviceAnswerAlarm eType, ref Int64 u8Account);

        //[DllImportAttribute(_dllPath, EntryPoint = "?HYGetLastError@@YG?AW4eQRErrorCode@@PBD@Z")]
        [DllImportAttribute(_dllPath, EntryPoint = "HYGetLastError")]
        [return: MarshalAsAttribute(UnmanagedType.I4)]
        public static extern eQRErrorCode HYGetLastError(string pDeviceSN);
    }
}
