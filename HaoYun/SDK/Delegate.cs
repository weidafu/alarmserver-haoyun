﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Xunmei.AlarmServer.Device.HaoYun
{
    //[System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    //public delegate bool HYDeviceMessage([System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string deviceSn, ref DeviceMessageType msg, ref Int64 u8Account, ref uint sLastEventTimeStamp, ref ushort sDeviceMemoryCRCCode, IntPtr cDeviceMemoryCRCIndex, IntPtr pDeviceInfo, ref int nDeviceInfoLen, ref ushort sZoneMemoryCRCCode, IntPtr cZoneMemoryCRCIndex, IntPtr pZoneInfo, ref int nZoneInfoLen);
    //[System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute(System.Runtime.InteropServices.CallingConvention.StdCall)]
    //public delegate bool HYDeviceMessage([System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string deviceSn, ref eDeviceMessageType msg, ref int u8Account, ref uint sLastEventTimeStamp, ref ushort sDeviceMemoryCRCCode, System.IntPtr cDeviceMemoryCRCIndex, System.IntPtr pDeviceInfo, ref int nDeviceInfoLen, ref ushort sZoneMemoryCRCCode, System.IntPtr cZoneMemoryCRCIndex, System.IntPtr pZoneInfo, ref int nZoneInfoLen);
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public delegate bool HYDeviceMessage([In, MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder deviceSn, [In] ref eDeviceMessageType msg, [In] ref long u8Account, ref ushort sEventSequence, ref ushort sDeviceMemoryCRCCode, ref byte cDeviceMemoryCRCIndex, [In, Out, MarshalAs(UnmanagedType.LPArray, SizeConst = 2048)]  byte[] pDeviceInfo, ref int nDeviceInfoLen, ref ushort sZoneMemoryCRCCode, ref byte cZoneMemoryCRCIndex, [In, Out, MarshalAs(UnmanagedType.LPArray, SizeConst = 2048)]  byte[] pZoneInfo, ref int nZoneInfoLen);


    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public delegate void HYDeviceEvent([In, MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder deviceSn, [In] ref eDeviceEventType eType, [In] ref ushort sEventSequence, [In] ref byte cZoneIndex, [In] ref long sOperator, [In] ref long tTimeStamp, bool bHistoryRecord);
}
