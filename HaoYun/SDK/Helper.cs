﻿using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Xunmei.AlarmServer.Device.HaoYun
{
    public partial class SDK
    {
        public static Int64 U8Account = 0;
        public static string LogPrefix = string.Empty;

        public static bool SetDeviceProperty(string deviceSN, eDevicePropertyName name, eDevicePropertyType type, int size, IntPtr ptr)
        {
            if (!HYSetDeviceProperty(deviceSN, ref name, ref type, ref size, ptr, ref U8Account))
            {
                if (LogError(deviceSN))
                    return false;
            }

            return true;
        }

        public static IntPtr GetDeviceProperty(string deviceSN, eDevicePropertyName name, ref int size)
        {
            StringBuilder sn = new StringBuilder(deviceSN);
            eDevicePropertyType type = eDevicePropertyType.DPTDeviceZonePropertyAndFault216GPlus;
            bool valid = false;
            int u8Account = 0;
            IntPtr ptr = SDK.HYGetDeviceProperty(sn, ref name, ref type, ref size, ref valid, ref u8Account);
            if (!valid)
            {
                if (LogError(deviceSN))
                    return IntPtr.Zero;
            }

            return ptr;
        }

        public static T[] GetPropertyValues<T>(string deviceSN, eDevicePropertyName propertyName)
        {
            int size = 0;
            IntPtr ptr = GetDeviceProperty(deviceSN, propertyName, ref size);
            if (ptr == IntPtr.Zero)
                return null;

            T qr = (T)Marshal.PtrToStructure(ptr, typeof(T));
            int len = Marshal.SizeOf(qr);
            int count = size / len;
            T[] qrs = new T[count];
            qrs[0] = qr;
            for (int i = 1; i < count; i++)
            {
                ptr = ptr.Offset(len);
                qrs[i] = (T)Marshal.PtrToStructure(ptr, typeof(T));
            }

            return qrs;
        }

        public static T GetPropertyValue<T>(string deviceSN, eDevicePropertyName propertyName)
        {
            int size = 0;
            IntPtr ptr = GetDeviceProperty(deviceSN, propertyName, ref size);
            if (ptr == IntPtr.Zero)
                return default(T);

            return (T)Marshal.PtrToStructure(ptr, typeof(T));
        }

        public static bool LogError(string deviceSN, string debugInfo = null)
        {
            string err = string.Empty;
            eQRErrorCode code = SDK.HYGetLastError(null);
            switch (code)
            {
                case eQRErrorCode.EECNormal:
                    err = "没有错误发生";
                    break;

                case eQRErrorCode.EECServerDontService:
                    err = "服务器没有启动服务";
                    break;

                case eQRErrorCode.EECOffline:
                    err = "设备离线";
                    break;

                case eQRErrorCode.EECDeviceUpgrading:
                    err = "设备正在升级中、不允许对设备进行任何布撤防、设置操作";
                    break;

                case eQRErrorCode.EECParameterError:
                    err = "设置参数错误";
                    break;

                case eQRErrorCode.EECUpgradeFileError:
                    err = "升级文件错误";
                    break;

                case eQRErrorCode.EECSameUpgradeVersion:
                    err = "设备版本当当前版本一致，不进行升级";
                    break;

                case eQRErrorCode.EECAlreadyStartTransmit:
                    err = "已经启动转发服务功能";
                    break;

                case eQRErrorCode.EECUnSupportFunction:
                    err = "不支持操作命令";
                    break;

                case eQRErrorCode.EECNoSelectedService:
                    err = "没有启动任何服务";
                    break;

                case eQRErrorCode.EECPortBinded:
                    err = "端口已经被绑定";
                    break;
            }

            if (code != eQRErrorCode.EECNormal)
            {
                Log.Error($"{LogPrefix}{err}错误代码{code}");
                return true;
            }

            if (!string.IsNullOrEmpty(debugInfo))
                Debug.WriteLine($"{LogPrefix}{debugInfo}");

            return false;
        }
    }
}