﻿using System;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Xunmei.AlarmServer.Common;

namespace Xunmei.AlarmServer.Device.HaoYun
{
    [Device("HaoYun", "浩云报警主机", typeof(SdkWrapper), typeof(AlarmDeviceEditForm), typeof(CommunicationEditor), typeof(CommunicationParameter))]
    public partial class Device : AlarmDevice
    {
        private CommunicationParameter _parameter = null;
        private DeviceInfo _deviceInfo = null;
        private string _logPrefix = string.Empty;
        private HYDeviceMessage _messageCallback = null;
        private HYDeviceEvent _eventCallback = null;
        private bool _isReady = false;
        private bool _isInited = false;

        public Device(IDeviceSdkWrapper sdkWrapper, DeviceInfo deviceInfo, SavedDeviceStateInfo savedState) : base(sdkWrapper, deviceInfo, savedState)
        {
            _deviceInfo = deviceInfo;
            _parameter = (new CommunicationParameter()).FromXml(deviceInfo.CommunicationParameterXml) as CommunicationParameter;
            SDK.LogPrefix = _logPrefix = $"浩云主机【{_deviceInfo.Name}】【{_parameter.DeviceSN}】：";
            _messageCallback = new HYDeviceMessage(MessageCallback);
            _eventCallback = new HYDeviceEvent(EventCallback);
        }

        #region AlarmDevice成员

        public override bool ArmZone(int zoneIndex, bool isArm)
        {
            if (zoneIndex > _deviceInfo.Zones.Length - 1)
            {
                Log.Error($"{_logPrefix}布撤防时防区索引{zoneIndex}越界。");
                return false;
            }

            qrZoneArm zoneArm;
            zoneArm.cZoneIndex = (byte)(zoneIndex + 1);
            zoneArm.cArmStatus = (byte)(isArm ? 1 : 0);
            if (!MarshalHelper.InvokeByPtr<qrZoneArm, bool>(zoneArm, new Method<bool>(ptr =>
             {
                 return SDK.SetDeviceProperty(_parameter.DeviceSN, eDevicePropertyName.DPNZoneArm, eDevicePropertyType.DPTZoneArm, Marshal.SizeOf(typeof(qrZoneArm)), ptr);
             })))
            {
                if (SDK.LogError(_parameter.DeviceSN, "布撤防异常。"))
                    return false;
            }

            return true;
        }

        public override void BypassInput(int moduleId, int inputIndex, bool bypass)
        {
            qrDeviceZonePropertyAndFault216GPlus[] properties = GetQrDeviceZoneProperties();
            if (properties == null)
                return;

            qrDeviceZonePropertyAndFault216GPlus property = properties.FirstOrDefault(p => p.cIndex == inputIndex + 1);
            property.bNeglect = (byte)(bypass ? 1 : 0);
            SetInput(property);
        }

        public override bool TurnOn(int moduleId, int outputIndex)
        {
            return TurnOutput(outputIndex, true);
        }

        public override bool TurnOff(int moduleId, int outputIndex)
        {
            return TurnOutput(outputIndex, false);
        }

        protected override bool ClearAlarm()
        {
            eDeviceAnswerAlarm answer = eDeviceAnswerAlarm.DAAOnlyAnswer;
            if (!SDK.HYDeviceAnswerAlarm(_parameter.DeviceSN, ref answer, ref SDK.U8Account))
            {
                if (SDK.LogError(_parameter.DeviceSN))
                    return false;
            }

            return true;
        }

        protected override string GetIP()
        {
            return _parameter.LocalIP;
        }

        protected override void Connect()
        {
            if (!_isInited)
            {
                bool isTranslateAlarm = true;
                int port = _parameter.LocalPort;
                bool isTranslateVideo = false;
                int portCameraCmd = 47624;
                int portCameraVideo = 47724;
                int portCameraAudio = 47824;
                try
                {
                    if (!SDK.HYInitialize(_parameter.LocalIP, ref isTranslateAlarm, ref port, ref isTranslateVideo, ref portCameraCmd, ref portCameraVideo, ref portCameraAudio, ref _messageCallback, ref _eventCallback))
                    {
                        SDK.LogError(_parameter.DeviceSN, "连接失败。");
                    }
                }
                catch (Exception ex)
                {
                }
            }

            _isInited = true;
        }

        protected override void Disconnect()
        {
            if (!SDK.HYDeInitialize())
            {
                SDK.LogError(_parameter.DeviceSN, "释放资源错误。");
            }
        }

        protected override AlarmZoneState[] QueryAlarmZoneState()
        {
            if (!_isReady)
                return base.QueryAlarmZoneState();

            qrZoneArm[] arms = GetZoneArms();
            AlarmZoneState[] states = new AlarmZoneState[arms.Length];
            foreach (qrZoneArm arm in arms)
                states[arm.cZoneIndex - 1] = arm.cArmStatus == 1 ? AlarmZoneState.Armed : AlarmZoneState.DisArmed;

            return states;
        }

        protected override AlarmInputState[] QueryBypassState(int channelCount)
        {
            return QueryInputState(channelCount);
        }

        protected override AlarmInputState[] QueryInputState(int channelCount)
        {
            if (!_isReady)
                return base.QueryInputState(channelCount);

            qrDeviceZonePropertyAndFault216GPlus[] propertis = SDK.GetPropertyValues<qrDeviceZonePropertyAndFault216GPlus>(_parameter.DeviceSN, eDevicePropertyName.DPN_WireZoneAndStatus);
            AlarmInputState[] inputStates = new AlarmInputState[propertis.Length];
            foreach (qrDeviceZonePropertyAndFault216GPlus property in propertis)
                inputStates[property.cIndex - 1] = GetInputState(property);

            return inputStates;
        }

        protected override OutputState[] QueryOutputState(int channelCount)
        {
            if (!_isReady)
                return base.QueryOutputState(channelCount);

            qrDeviceZoneFault fault = GetFault();
            OutputState[] states = new OutputState[4];
            states[0] = (fault.bFaultZone & 0x10) == 1 ? OutputState.On : OutputState.Off;
            states[1] = (fault.bFaultZone & 0x20) == 1 ? OutputState.On : OutputState.Off;
            states[2] = (fault.bFaultZone & 0x40) == 1 ? OutputState.On : OutputState.Off;
            states[3] = (fault.bFaultZone & 0x80) == 1 ? OutputState.On : OutputState.Off;

            return states;
        }

        #endregion

        #region 私有方法
        private bool TurnOutput(int outputIndex, bool isOn)
        {
            /*eDevicePropertyName name = eDevicePropertyName.DPNOrdinaryRelay;
            eDevicePropertyType type = eDevicePropertyType.DPTDeviceZoneFault;
            qrDeviceZoneFault fault = new qrDeviceZoneFault();
            fault.bFaultZone = 0x04;
            IntPtr ptr = Marshal.AllocHGlobal(Marshal.SizeOf(fault));
            Marshal.StructureToPtr(fault, ptr, true);
            int size = 1;
            bool ret = SDK.HYSetDeviceProperty(_parameter.DeviceSN, ref name, ref type, ref size, ptr, ref SDK.U8Account);
            Marshal.FreeHGlobal(ptr);*/

            string operation = isOn ? "打开" : "关闭";
            string log = $"{_logPrefix}{operation}继电器时输出索引{outputIndex}";

            if (outputIndex > 3)
            {
                Log.Error($"{log}越界。");
                return false;
            }

            qrDeviceZoneFault fault = new qrDeviceZoneFault();
            foreach (OutputStatePair<OutputState> pair in _stateInfo.OutputStates)
            {
                if (pair.Value == OutputState.On)
                    fault.bFaultZone |= (byte)(0x01 << pair.Output.Index);
            }
            byte val = (byte)(0x01 << outputIndex);
            if (isOn)
                fault.bFaultZone |= val;
            else
                fault.bFaultZone &= (byte)~val;
            if (!MarshalHelper.InvokeByPtr<qrDeviceZoneFault, bool>(fault, new Method<bool>(ptr =>
            {
                return SDK.SetDeviceProperty(_parameter.DeviceSN, eDevicePropertyName.DPNOrdinaryRelay, eDevicePropertyType.DPTDeviceZoneFault, Marshal.SizeOf(fault), ptr);
            })))
            {
                if (SDK.LogError(_parameter.DeviceSN, $"{log}异常。"))
                    return false;
            }

            return true;
        }

        private bool MessageCallback([In, MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder deviceSn, [In] ref eDeviceMessageType msg, [In] ref long u8Account, ref ushort sEventSequence, ref ushort sDeviceMemoryCRCCode, ref byte cDeviceMemoryCRCIndex, [In, Out, MarshalAs(UnmanagedType.LPArray, SizeConst = 2048)]  byte[] pDeviceInfo, ref int nDeviceInfoLen, ref ushort sZoneMemoryCRCCode, ref byte cZoneMemoryCRCIndex, [In, Out, MarshalAs(UnmanagedType.LPArray, SizeConst = 2048)] byte[] pZoneInfo, ref int nZoneInfoLen)
        {
            if (deviceSn.ToString() != _parameter.DeviceSN)
                return true;

            string desc = msg.GetDescription<eDeviceMessageType>();
            ReportDeviceEvent(desc);
            desc = GetRealDesc(desc);
            Debug.WriteLine(desc);
            switch (msg)
            {
                case eDeviceMessageType.DMTLogin:
                    OnConnectStateChanged(true, desc);
                    return false;

                case eDeviceMessageType.DMTLogout:
                    OnConnectStateChanged(false, desc);
                    break;

                case eDeviceMessageType.DMTZoneArmChange:
                    HandleZoneArmChanged(GetZoneArms());
                    break;

                case eDeviceMessageType.DMTExpanderDevicesStatusChanged:
                    HandleOrdinaryRelayChanged();
                    break;

                case eDeviceMessageType.DMTZoneStatusChanged:
                    HandlePropertyChanged();
                    if (!_isReady)
                    {
                        SetZone();
                        _isReady = true;
                    }
                    break;
            }

            return true;
        }

        private void EventCallback([In, MarshalAsAttribute(UnmanagedType.LPStr)] StringBuilder deviceSn, [In] ref eDeviceEventType eType, [In] ref ushort sEventSequence, [In] ref byte cZoneIndex, [In] ref long sOperator, [In] ref long tTimeStamp, bool bHistoryRecord)
        {
            if (deviceSn.ToString() != _parameter.DeviceSN)
                return;

            string desc = eType.GetDescription<eDeviceEventType>();
            Debug.WriteLine(desc);
            ReportDeviceEvent(eType.GetDescription<eDeviceEventType>());
        }

        private void ReportDeviceEvent(string desc)
        {
            if (!desc.StartsWith("*"))
                return;

            string realDesc = string.Empty;
            DeviceEventType[] types = ParseDesc(desc, out realDesc);
            foreach (DeviceEventType type in types)
                OnDeviceEventOccured(type, DateTime.Now, realDesc, null);
        }

        private DeviceEventType[] ParseDesc(string desc, out string realDesc)
        {
            int index = desc.IndexOf("：");
            realDesc = desc.Substring(index + 1);

            string[] names = desc.Substring(1, index - 1).Split("|".ToCharArray());
            DeviceEventType[] types = new DeviceEventType[names.Length];
            int i = 0;
            foreach (string name in names)
                types[i++] = (DeviceEventType)Enum.Parse(typeof(DeviceEventType), name);

            return types;
        }

        private string GetRealDesc(string desc)
        {
            string realDesc = string.Empty;
            DeviceEventType[] types = ParseDesc(desc, out realDesc);

            return realDesc;
        }

        /*private void HandleLogin(ref ushort sDeviceMemoryCRCCode, ref byte cDeviceMemoryCRCIndex, [In, Out, MarshalAs(UnmanagedType.LPArray, SizeConst = 2048)]  byte[] pDeviceInfo, ref int nDeviceInfoLen, ref ushort sZoneMemoryCRCCode, ref byte cZoneMemoryCRCIndex, [In, Out, MarshalAs(UnmanagedType.LPArray, SizeConst = 2048)] byte[] pZoneInfo, ref int nZoneInfoLen)
        {
            OnConnectStateChanged(true, string.Empty);
            //TODO: 确认以下代码必要性
            if (pDeviceInfo != null)
            {
                sDeviceMemoryCRCCode = 0;
                cDeviceMemoryCRCIndex = 0;
                nDeviceInfoLen = 201;
                for (int i = 0; i < pDeviceInfo.Length; i++)
                    pDeviceInfo[i] = 0xFF;

                sZoneMemoryCRCCode = 0;
                cZoneMemoryCRCIndex = 0x0;
                nZoneInfoLen = 107 * 5;
                for (int i = 0; i < pZoneInfo.Length; i++)
                    pZoneInfo[i] = 0xFF;
            }
        }*/

        private void HandleZoneArmChanged(qrZoneArm[] arms)
        {
            if (arms == null)
                return;

            foreach (qrZoneArm arm in arms)
                OnAlarmZoneStateChanged(arm.cZoneIndex - 1, arm.cArmStatus == 0 ? AlarmZoneState.DisArmed : AlarmZoneState.Armed);

            OnConnectStateChanged(true, string.Empty);
        }

        /*private void HandleZoneNeglectChanged(qrDeviceZoneTempNeglect[] neglects)
        {
            if (neglects == null)
                return;

            foreach (qrDeviceZoneTempNeglect neglect in neglects)
                OnInputStateChanged(neglect.cIndex, neglect.cNeglect == 1 ? AlarmInputState.Bypass : AlarmInputState.Normal);
        }*/

        private void HandleOrdinaryRelayChanged()
        {
            qrDeviceZoneFault fault = GetFault();
            OnOutputStateChanged(0, (fault.bFaultZone & 0x10) > 0 ? OutputState.On : OutputState.Off);
            OnOutputStateChanged(1, (fault.bFaultZone & 0x20) > 0 ? OutputState.On : OutputState.Off);
            OnOutputStateChanged(2, (fault.bFaultZone & 0x40) > 0 ? OutputState.On : OutputState.Off);
            OnOutputStateChanged(3, (fault.bFaultZone & 0x80) > 0 ? OutputState.On : OutputState.Off);
        }

        private void HandlePropertyChanged()
        {
            qrDeviceZonePropertyAndFault216GPlus[] properties = GetQrDeviceZoneProperties();
            if (properties == null)
                return;

            foreach (qrDeviceZonePropertyAndFault216GPlus property in properties)
                OnInputStateChanged(property.cIndex - 1, GetInputState(property));
        }

        private AlarmInputState GetInputState(qrDeviceZonePropertyAndFault216GPlus property)
        {
            AlarmInputState state = AlarmInputState.Normal;
            //if (property.bEnabled == 1)//TODO：为啥是0
            //    state = AlarmInputState.Normal;
            if (property.bNeglect == 1)
                state = AlarmInputState.Bypass;

            return state;
        }

        private qrZoneArm[] GetZoneArms()
        {
            return SDK.GetPropertyValues<qrZoneArm>(_parameter.DeviceSN, eDevicePropertyName.DPNZoneArm);
        }

        private qrDeviceZoneTempNeglect[] GetNelects()
        {
            return SDK.GetPropertyValues<qrDeviceZoneTempNeglect>(_parameter.DeviceSN, eDevicePropertyName.DPNZoneTempNeglect);
        }

        private qrDeviceZoneFault GetFault()
        {
            return SDK.GetPropertyValue<qrDeviceZoneFault>(_parameter.DeviceSN, eDevicePropertyName.DPN_DevicePartsStatus);
        }

        private void SetZone()
        {
            qrDeviceZonePropertyAndFault216GPlus[] properties = GetQrDeviceZoneProperties();
            if (properties == null)
                return;

            foreach (ZoneInfo zone in _deviceInfo.Zones)
            {
                foreach (InputInfo input in zone.Inputs)
                {
                    qrDeviceZonePropertyAndFault216GPlus property = properties.FirstOrDefault(p => p.cIndex == input.Index + 1);
                    if (property.cBelongPartition == zone.Index + 1)
                        continue;

                    property.cBelongPartition = (byte)(zone.Index + 1);
                    SetInput(property);
                }
            }
        }

        private qrDeviceZonePropertyAndFault216GPlus[] GetQrDeviceZoneProperties()
        {
            return SDK.GetPropertyValues<qrDeviceZonePropertyAndFault216GPlus>(_parameter.DeviceSN, eDevicePropertyName.DPN_WireZoneAndStatus);
        }

        private void SetInput(qrDeviceZonePropertyAndFault216GPlus property)
        {
            if (!MarshalHelper.InvokeByPtr<qrDeviceZonePropertyAndFault216GPlus, bool>(property, new Method<bool>(ptr =>
            {
                return SDK.SetDeviceProperty(_parameter.DeviceSN, eDevicePropertyName.DPNWiredZoneProperty, eDevicePropertyType.DPTDeviceZonePropertyAndFault216GPlus, Marshal.SizeOf(property), ptr);
            })))
            {
                SDK.LogError(_parameter.DeviceSN, "设置输入异常。");
            }
        }

        #endregion
    }
}
