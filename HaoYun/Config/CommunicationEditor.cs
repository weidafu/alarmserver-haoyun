﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;

using Xunmei.AlarmServer.Common;

namespace Xunmei.AlarmServer.Device.HaoYun
{
    /// <summary>
    /// B9512G-CHI_报警主机网络参数配置页面。
    /// </summary>
    public partial class CommunicationEditor : CommunicationEditorBase
    {
        private CommunicationParameter _param = null;

        public CommunicationEditor(string xml)
        {
            InitializeComponent();

            string[] ips = Net.GetIPs();
            foreach (string ip in ips)
                cbxIPs.Items.Add(ip);

            if (string.IsNullOrEmpty(xml))
            {
                _param = new CommunicationParameter();
                cbxIPs.SelectedIndex = 0;
                return;
            }

            _param = (new CommunicationParameter()).FromXml(xml) as CommunicationParameter;
            cbxIPs.Text = _param.LocalIP;
            nudPort.Value = _param.LocalPort;
            txtDeviceSN.Text = _param.DeviceSN;
            txtDeviceIP.Text = _param.NetAddress;
        }

        #region 重写方法

        /// <summary>
        /// 验证参数。
        /// </summary>
        /// <returns>通过验证返回true，否则返回false。</returns>
        public override bool ValidateParameters()
        {
            if (!ValidateNetAddress())
                return false;

            return true;
        }

        /// <summary>
        /// 获取网络参数XML。（调用前，需调用ValidateParameters方法验证。）
        /// </summary>
        /// <returns></returns>
        public override string GetCommunicationParameterXML()
        {
            _param.LocalIP = cbxIPs.Text;
            _param.LocalPort = ushort.Parse(nudPort.Value.ToString());
            _param.DeviceSN = txtDeviceSN.Text;
            _param.NetAddress = txtDeviceIP.Text;

            return XmlHelper.ToXML<CommunicationParameter>(_param);
        }

        #endregion

        #region 私有方法

        /// <summary>
        /// 验证网络地址。
        /// </summary>
        /// <returns>通过验证返回true，否则返回false。</returns>
        private bool ValidateNetAddress()
        {
            errorProvider.Clear();
            string ip = cbxIPs.Text;
            if (String.IsNullOrEmpty(ip))
            {
                errorProvider.SetError(cbxIPs, "请选择IP地址。");
                return false;
            }

            return true;
        }

        #endregion
    }
}
