﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

using Xunmei.AlarmServer.Common;

namespace Xunmei.AlarmServer.Device.HaoYun
{
    public class CommunicationParameter : CommunicationParameterBase
    {
        public string DeviceSN
        {
            get; set;
        }

        public string LocalIP
        {
            get;set;
        }

        public int LocalPort
        {
            get; set;
        }

        public override CommunicationParameterBase FromXml(string xml)
        {
            return XmlHelper.Parse<CommunicationParameter>(xml);
        }
    }
}
