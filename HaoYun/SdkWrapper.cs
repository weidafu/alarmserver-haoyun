﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xunmei.AlarmServer.Common;
using Xunmei.AlarmServer.Device;

namespace Xunmei.AlarmServer.Device.HaoYun
{
    public partial class Device : AlarmDevice
    {
        public class SdkWrapper : IDeviceSdkWrapper
        {
            private Device _device = null;

            public string Vendor
            {
                get
                {
                    return "HaoYun";
                }
            }

            public string Version
            {
                get
                {
                    return "v1.0.0.0";
                }
            }

            public void DeInit()
            {                
            }

            public bool Initialize()
            {
                return true;
            }

            internal Device Device
            {
                set
                {
                    _device = value;
                }
            }
        }
    }
}
