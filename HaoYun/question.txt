/*主机基础信息*/
typedef struct qrHostBasicInfo
{
	/*子系统数量*/
	unsigned char cSubsystemNum;
	/*扩展模块数量*/
	unsigned char cExpansionModuleNum;
	/*防区数量*/
	unsigned char cZoneNum;
	/*报警输出数量*/			这里报警输出与继电器有何区别？
	unsigned char cAlarmOutNum;
	/*特殊用途项（1，催泪瓦斯）*/
	unsigned char cSpecialValue;
}QRHOSTBASICINFO, *LPQRHOSTBASICINFO;

/*扩展模块信息*/
typedef struct qrExpansionModuleInfo
{
	/*模块编号*/
	unsigned char cModuleNum;
	/*输入防区数量*/
	unsigned char cZoneInNum;
	/*输出防区数量*/			这里输出防区数量是啥？
	unsigned char cZoneOutNum;
}QREXPANSIONMODULEINFO, *LPQREXPANSIONMODULEINFO;
